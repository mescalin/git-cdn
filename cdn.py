#!/usr/bin/env python
#coding=utf-8

import hashlib
import mimetypes
import pygit2
import string
import web

import log

_PATH = '/home/vagrant/work/project/new-git-cdn/arpgResource/'
_REPO = pygit2.Repository(_PATH + '.git')

class Nothing:
    def GET(self, name):
        return '\n'.join([repr(e) for e in web.ctx.iteritems()])

def recursiveTree(l, tree, name):
    for entry in tree:
        obj = _REPO.get(entry.id)
        if obj.type == pygit2.GIT_OBJ_TREE:
            child = name + '/' + entry.name
            if child[0] == '/':
                child = child[1:]
            hash = hashlib.sha1(child).digest()
            l.append(hash[:5] + entry.id.raw)
            recursiveTree(l, obj, child)

class Index:
    def GET(self, tagname):
        l = []
        tree = _REPO.lookup_reference('refs/tags/' + tagname).peel().tree
        recursiveTree(l, tree, '')
        l.append(hashlib.sha1('.').digest()[:5] + tree.id.raw)
        return ''.join(l)

class Diff:
    def GET(self, tags):
        tags = string.split(tags, '..')
        diff = _REPO.diff('refs/tags/' + tags[0], 'refs/tags/' + tags[1])
        l = []
        for patch in diff:
            log.logger.error(patch.new_file_path + ':' + patch.old_id.hex)
            l.append(hashlib.sha1(patch.new_file_path).digest()[:5] + patch.new_id.raw)
        return ''.join(l)

class Tree:
    def GET(self, hash, filename):
        entry = _REPO.get(hash)
        if entry.type == pygit2.GIT_OBJ_COMMIT:
            entry = entry.tree
        data = _REPO.get(entry[filename].id).data
        m = mimetypes.guess_type(filename)
        if m[0]:
            web.header('Content-Type', m[0])
        web.header('Content-Length', len(data))
        return data

class File:
    def GET(self, hash, filename):
        data = _REPO.get(hash).data
        m = mimetypes.guess_type(filename)
        if m[0]:
            web.header('Content-Type', m[0])
        web.header('Content-Length', len(data))
        return data

class Cross:
    def GET(self):
        web.header('Content-Type', 'application/xml')
        return '''<cross-domain-policy>
<allow-access-from domain="*" to-ports="*"/>
<allow-access-from domain="localhost" to-ports="*"/>
</cross-domain-policy>'''

class FileLoader:
    def GET(self, filename):
        f = open(_PATH + filename)
        data = f.read()
        web.header('Content-Length', len(data))
        m = mimetypes.guess_type(filename)
        if m[0]:
            web.header('Content-Type', m[0])
        f.close()
        return data

_URLS = (
    '/mt/index/(.+)', Index,
    '/mt/diff/(.+)', Diff,
    '/mt/tree/(.+)/(.+)', Tree,
    '/mt/file/(.+)/(.+)', File,
    '/mt/(.+)', FileLoader,
    '/crossdomain.xml', Cross,
    '/(.*)', Nothing,
)

application = web.application(_URLS, globals()).wsgifunc()

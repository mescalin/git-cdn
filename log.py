#!/usr/bin/env python
#coding=utf-8

import logging

_fh = logging.FileHandler('./cdn.log')
_fh.setLevel(logging.INFO)
_fh.setFormatter(logging.Formatter('%(asctime)s | %(levelno)s | %(filename)s(%(lineno)d), %(funcName)s | %(message)s'))

logger = logging.getLogger('simple_example')
logger.setLevel(logging.INFO)
logger.addHandler(_fh)
